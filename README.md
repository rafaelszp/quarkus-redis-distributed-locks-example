# redis-distributed-locks project

This project has 3 objectives

1. Show an example of usage of a distributed semaphore/limiter for concurrent logins, eg `LimitedLoginService` 
1. Show an example of usage of a distributed lock for a file appender
1. Show an example of usage of a distributed mutex

### Distributed Lock

1. See [src/main/java/szp/rafael/rdl/fileappender/FileAppenderService.java](./src/main/java/szp/rafael/rdl/fileappender/FileAppenderService.java)
1. See also:  [src/test/java/szp/rafael/rdl/fileappender/FileAppenderServiceTest.java](./src/test/java/szp/rafael/rdl/fileappender/FileAppenderServiceTest.java)
1. In this example I show a way to lock a write/append content to a specific file which can only be written by one process at a time;

**Obs.:** Locks can only be unlocked by their owner. In this example I set an expiration so the lock  can be released in cases of an application crash/shutdown.

### Distributed Mutex

1.See [src/main/java/szp/rafael/rdl/exampleSchedule/ExampleScheduledService.java](./src/main/java/szp/rafael/rdl/exampleSchedule/ExampleScheduledService.java)
1. See also: [src/test/java/szp/rafael/rdl/exampleSchedule/ExampleScheduledServiceTest.java](./src/test/java/szp/rafael/rdl/exampleSchedule/ExampleScheduledServiceTest.java)
1. In this example I show a way to implement a Quarkus Scheduler (e.g. `@Scheduler`) so the job can only be executed by only one instance at time
    - Mutexes should expire after a predefined time (e.g. `MUTEX_EXPIRATION_SECONDS` variable) 

**Obs.:** Mutex is a type of semaphore, but with only one permit.

### Distributed semaphore

1. See [src/main/java/szp/rafael/rdl/login/LimitedLoginService.java](./src/main/java/szp/rafael/rdl/login/LimitedLoginService.java)
1. See also: [src/test/java/szp/rafael/rdl/login/LimitedLoginServiceTest.java](./src/test/java/szp/rafael/rdl/login/LimitedLoginServiceTest.java)
1. In this example I show a way to implement a login limiter, e.g. concurrent logins with same username, using an expirable semaphore with 2 permits

## Appendix

1. Java Locks: https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Lock.html
1. Java Semaphores: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/Semaphore.html
1. RedissonDistributed locks and synchronizers: https://github.com/redisson/redisson/wiki/8.-Distributed-locks-and-synchronizers


