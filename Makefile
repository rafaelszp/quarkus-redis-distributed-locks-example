.PHONY: redis.restart redis.run redis.stop

redis.start:
	docker run --ulimit memlock=-1:-1 --rm=true  --memory-swappiness=0   --name redis-cache  -p 6379:6379 -d redis:5.0.6  redis-server --appendonly no

redis.stop:
	docker stop redis-cache

redis.connect:
	docker exec -it redis-cache redis-cli



