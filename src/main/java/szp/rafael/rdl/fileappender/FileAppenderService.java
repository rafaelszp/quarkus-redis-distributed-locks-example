package szp.rafael.rdl.fileappender;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.logging.Logger;

@Singleton
public class FileAppenderService {

  @Inject
  RedissonClient redisson;

  Logger logger = Logger.getLogger("FileWriterService");

  public boolean appendToFile(String content) throws InterruptedException {

    String fileName = this.getClass().getClassLoader().getResource("./").getFile() + "my_file.log";
    boolean didWrite = false;

    RLock lock = redisson.getLock("my_file_lock");
    boolean isReleased = lock.tryLock();
    if (isReleased) {


      try {

        String contentToAppend = "[" + LocalDateTime.now() + "] - [INFO] - " + content + "\r\n";

        Path path = Paths.get(fileName);
        if (!Files.exists(path)) {

          Files.createFile(path);
        }
        Files.write(
          path,
          contentToAppend.getBytes(),
          StandardOpenOption.APPEND);
      } catch (IOException e) {
        logger.warning("Error on appending file - " + e.getMessage());
      }
      Thread.sleep(1000);

      lock.unlock();
      didWrite = true;
      logger.info("File written sucessfully: " + fileName);
    } else {
      logger.info("File locked");
    }
    return didWrite;
  }


}
