package szp.rafael.rdl.exampleSchedule;

import io.quarkus.scheduler.Scheduled;
import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.api.RedissonClient;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ExampleScheduledService {

  final static int MUTEX_EXPIRATION_SECONDS=9; //This value should be around the max execution time of the job.
  Logger logger = Logger.getLogger(this.getClass().getName());
  @Inject
  RedissonClient redissonClient;

  protected RPermitExpirableSemaphore mutex;

  void setupMutex() {
    mutex = redissonClient.getPermitExpirableSemaphore("example_job");
    mutex.trySetPermits(1);//Number of permits. In this case only once, because the doWork method cannot be executed by two or more instances at a time.
    mutex.expire(MUTEX_EXPIRATION_SECONDS,TimeUnit.SECONDS); // it's important the mutex to expire so the job can be rerun in case of a previous crash.
  }

  /*
  * This example job calls the method doWork only when there is any available permit to execute the job.
  * In case of an environment with multiple application instances, a job cannot be executed more than once.
  * So you'll need a distributed semaphore/mutex to permit only one execution at a time.
  */
  @Scheduled(cron = "{example_job.cron}")
  void exampleJob() {
    setupMutex();
    logger.info("Trying to execute the current job with available permits: " + mutex.availablePermits());

    String id = null;
    try {
      id = mutex.tryAcquire(1, MUTEX_EXPIRATION_SECONDS, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      logger.severe("Acquire mutex error: "+e.getMessage());
    }
    if(id != null){
      doWork(id);
    }
  }

  void doWork(String id){
    try {
      logger.info("Executing job Work");
      Thread.sleep(MUTEX_EXPIRATION_SECONDS *1000);
      mutex.tryRelease(id); // releasing permits so the job can be executed again in a next time window by another thread
      logger.info("Job executed. Available permits: "+mutex.availablePermits());
    }catch(Exception e){
      logger.warning(e.getMessage()+": "+e.getCause().getMessage());
    }
  }


  /* In this case the semaphore will be released after a timeout of leaseTime seconds.
  When timeout is reached while the application crashed/stopped before the release, then the semaphore will be available after lease time expiration */
  public void wontReleaseMutex(){
    RPermitExpirableSemaphore unreleasedMutex = redissonClient.getPermitExpirableSemaphore("unreleased_mutex");
    unreleasedMutex.trySetPermits(1);
    try {
      int wait = 1;
      int leaseTime = 2;
      final String id = unreleasedMutex.tryAcquire(wait, leaseTime, TimeUnit.SECONDS);
      logger.info("Unreleased mutex id "+id + ", permits: "+unreleasedMutex.availablePermits());
    } catch (Exception e) {
      logger.severe(e.getMessage());
    }
  }
}