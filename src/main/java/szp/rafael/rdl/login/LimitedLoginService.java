package szp.rafael.rdl.login;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.api.RedissonClient;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Singleton
public class LimitedLoginService {


  Logger logger = Logger.getLogger(this.getClass().getName());

  @Inject
  RedissonClient redissonClient;

  @ConfigProperty(name = "concurrent_logins.count")
  Integer concurrentLogins;

  @ConfigProperty(name = "concurrent_logins.semaphore_timeout_seconds")
  Integer semaphoreTimeout;


  public String login(String username) {

    RPermitExpirableSemaphore loginLimiter = getSemaphore(username);
    boolean isSet = loginLimiter.trySetPermits(concurrentLogins); //if doesn't exist, create with concurrentLogins value
    if (isSet) {
      loginLimiter.expire(semaphoreTimeout, TimeUnit.SECONDS);
    }
    String id = loginLimiter.tryAcquire();
    if (id != null) {
      processLogin(id);
      logger.info(String.format("new slots count for %s: %s",username,loginLimiter.availablePermits()));
    }
    return id;

  }

  private RPermitExpirableSemaphore getSemaphore(String username) {
    return redissonClient.getPermitExpirableSemaphore("limited_login:" + username);
  }

  public void processLogin(String semaphoreId) {
    logger.info("Logged in with semaphoreId: "+semaphoreId);
  }

  public boolean logout(String username, String lockId) {
    RPermitExpirableSemaphore loginLimiter = getSemaphore(username);
    boolean result = loginLimiter.tryRelease(lockId);
    logger.info("Logout result of id: "+lockId +": "+result);
    logger.info(String.format("new slots count for %s: %s",username,loginLimiter.availablePermits()));
    return result;
  }


}
