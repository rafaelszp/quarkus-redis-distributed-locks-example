package szp.rafael.rdl.redis;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.api.RedissonReactiveClient;
import org.redisson.config.Config;

import javax.enterprise.context.ApplicationScoped;

public class RedissonProducer {


  @ConfigProperty(name = "redis.host")
  public String hosts;


  @ApplicationScoped
  public RedissonClient createRedisson() {
    Config config = new Config();
    config.useSingleServer().setAddress(hosts);
    return Redisson.create(config);
  }

  @ApplicationScoped
  public RedissonReactiveClient createRedissonReactive() {
    Config config = new Config();
    config.useSingleServer().setAddress(hosts);
    return Redisson.createReactive(config);
  }

}
