package szp.rafael.rdl.exampleSchedule;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.api.RedissonClient;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class ExampleScheduledServiceTest {

  @Inject
  ExampleScheduledService service;

  @Inject
  RedissonClient redissonClient;

  @Test
  public void should_set_permits_only_once(){
    service.setupMutex();
    service.mutex.trySetPermits(2); //Will not set because it was set once in @PostConstruct of ExampleScheduledService
    assertEquals(1,service.mutex.availablePermits());
  }

  @Test
  public void should_execute_job() throws Exception{

    service.exampleJob();
    assertEquals(0, service.mutex.availablePermits());

    RPermitExpirableSemaphore example_job = redissonClient.getPermitExpirableSemaphore("example_job");
    example_job.trySetPermits(1);
    example_job.expire(10, TimeUnit.SECONDS);
    assertTrue(example_job.availablePermits()>0);

  }

  @Test
  public void should_recover_an_unreleased_mutex() throws Exception{
    service.wontReleaseMutex();
    Thread.sleep(2000);
    RPermitExpirableSemaphore mutex = redissonClient.getPermitExpirableSemaphore("unreleased_mutex");
    int perm = mutex.availablePermits();
    assertTrue(perm>0);
    mutex.expire(1,TimeUnit.MILLISECONDS);
  }

}
