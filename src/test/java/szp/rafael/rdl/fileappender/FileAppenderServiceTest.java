package szp.rafael.rdl.fileappender;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
class FileAppenderServiceTest {


  @Inject
  FileAppenderService service;

  @Test
  void should_append_to_file() throws IOException, InterruptedException {
    boolean test = service.appendToFile("test");
    System.out.println(test);
  }

  @Test
  void should_write_only_once() throws Exception {
    ExecutorService executor = Executors.newFixedThreadPool(2);
    Future<Boolean> test1 = execute(executor);
    Future<Boolean> test2 = execute(executor);
    Boolean t1 = test1.get();
    System.out.println("test1 " + t1);
    Boolean t2 = test2.get();
    System.out.println("test2 " + t2);
    assertTrue(t1 ^ t2);
  }

  private Future<Boolean> execute(ExecutorService executor) {
    return executor.submit(() -> {
      try {
        boolean test = service.appendToFile("test with thread " + Thread.currentThread().getName());
        System.out.println("Result of " + Thread.currentThread().getName() + " - " + test);
        return test;
      } catch (InterruptedException e) {
        return false;
      }
    });
  }


}