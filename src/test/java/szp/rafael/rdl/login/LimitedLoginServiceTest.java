package szp.rafael.rdl.login;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class LimitedLoginServiceTest {

  @Inject
  LimitedLoginService loginService;


  @ConfigProperty(name="concurrent_logins.semaphore_timeout_seconds")
  Integer timeout;

  @Test
  void should_do_login_only_twice() throws Exception {
    String t1 = loginService.login("teste");
    String t2 = loginService.login("teste");
    String t3 = loginService.login("teste");
    assertNotEquals(t1,t2);
    assertNull(t3);
  }

  @Test
  void should_wait_to_expire_and_try_to_login() throws Exception{
    String t1 = loginService.login("teste");
    String t2 = loginService.login("teste");
    String t3 = loginService.login("teste");
    assertFalse(t1.equals(t2) && t1.equals(t3));
    assertNull(t3);
    Thread.sleep(timeout*1000);
    String t4 = loginService.login("teste");
    assertNotNull(t4);
  }


  @Test
  void should_logout(){
    String username = "teste_logout";
    String id = loginService.login(username);
    boolean logout = loginService.logout(username, id);
    assertTrue(logout);
  }

  @Test
  public void should_release_and_retry_login() {
    String username = "teste_login_logout";
    String t1 = loginService.login(username);
    String t2 = loginService.login(username);
    String t3 = loginService.login(username);
    assertFalse(t1.equals(t2) && t1.equals(t3));
    assertNull(t3);

    boolean didLogout = loginService.logout(username, t1);
    assertTrue(didLogout);

    String t4 = loginService.login(username);
    assertNotNull(t4);
    assertFalse(t1.equals(t2) && t1.equals(t3) && t1.equals(t4));
    boolean t4Logout = loginService.logout(username, t4);
    assertTrue(t4Logout);


  }

}